FROM ubuntu:18.04

LABEL maintainer="ed.henderson@peigenesis.com"

RUN \
    apt-get update && \
    apt-get install -y software-properties-common && \
    add-apt-repository ppa:openjdk-r/ppa && \
    apt-get update && \
    apt-get install --no-install-recommends -y openjdk-8-jdk maven

# Add the following line when I'm ready to "trim" the container
#RUN rm -rf /var/lib/apt/lists/*

#COPY apache-ant-1.9.15 /usr/local
#COPY apache-maven-3.2.5 /usr/local
COPY jdk1.6.0_45/ /usr/local/jdk1.6.0_45
COPY .bashrc /root
COPY .bash_aliases /root

ENV JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64
ENV LANG UTF-8
ENV LC_ALL en_US.UTF-8
ENV TZ "America/New_York"
ENV TERM xterm

CMD ["bash"]
